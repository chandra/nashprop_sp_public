#ifndef _PLAYER
#define _PLAYER
#include <vector>
#include <tuple>
#include "JointStrategySet.hpp"

class Player {
public:
  short index;
  //actual strategy will be the value below divided by GRID_SIZE
  strategy strat;
  short num_nbrs;
  std::vector<short> nbrs;
  //TODO - change below vector to array for speedup
  std::vector<short> unset_nbrs_shuffled;//to access the unset neighbours in a random manner
  std::vector<short> set_nbrs;//to access the neighbours who have alaredy played
  //set of best response joint strategies
  JointStrategySet B;
  std::vector<double>* payoff;//payoff matrix
  Player();
  //creates the set of best response joint strategies
  void pop_joint_action_BR_set(Player* players);
  //get epsilon best response of this player given a joint action of nbrs
  std::tuple<short,short> get_BR_i(JointStrategy* j_strat);
  void print_player() const;
  ~Player();
};

#endif
