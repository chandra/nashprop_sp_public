#include <iostream>
#include <cmath>
#include "JointStrategy.hpp"

using namespace std;
extern short GRID_SIZE;

JointStrategy::JointStrategy() {
  joint_action=new map<short,strategy>();
  cross_prod_pos=0;
}

JointStrategy::JointStrategy(map<short,strategy>* joint_action) {
  this->joint_action=joint_action;
  if(joint_action->size()==2)
    calc_cross_prod_pos();
  else
    cross_prod_pos=0;
}

JointStrategy::JointStrategy(const JointStrategy& j_strat) {
  joint_action=new map<short,strategy>();
  map<short,strategy>::const_iterator begin=j_strat.joint_action->begin();
  map<short,strategy>::const_iterator end=j_strat.joint_action->end();
  for(;begin!=end;begin++) {
    (*joint_action)[begin->first]=begin->second;
  }
  cross_prod_pos=j_strat.cross_prod_pos;
}

//Move constructor
JointStrategy::JointStrategy(JointStrategy&& j_strat) {
  joint_action=j_strat.joint_action;
  cross_prod_pos=j_strat.cross_prod_pos;
  j_strat.joint_action=nullptr;
  j_strat.cross_prod_pos=0;
}

JointStrategy& JointStrategy::operator=(const JointStrategy& other) {
  if(this!=&other) {
    joint_action->clear();
    joint_action->insert(other.joint_action->begin(),other.joint_action->end());
    cross_prod_pos=other.cross_prod_pos;
  }
  return *this;
}

//Move assignment operator
JointStrategy& JointStrategy::operator=(JointStrategy&& other) {
  map<short,strategy>* tempptr=joint_action;
  tuple_index temp=cross_prod_pos;
  joint_action=other.joint_action;
  cross_prod_pos=other.cross_prod_pos;
  other.joint_action=tempptr;
  other.cross_prod_pos=temp;  
  return *this;
}

strategy JointStrategy::get_player_strat(short player) const {
  return joint_action->find(player)->second;
}

bool JointStrategy::operator==(const JointStrategy& other) const {
  if(joint_action->size()==other.joint_action->size()) {
    map<short,strategy>::const_iterator self_begin=joint_action->begin();
    map<short,strategy>::const_iterator self_end=joint_action->end();
    map<short,strategy>::const_iterator other_begin=other.joint_action->begin();
    for(;self_begin!=self_end;self_begin++,other_begin++) {
      if(self_begin->first!=other_begin->first || self_begin->second!=other_begin->second)
    	  return false;
    }
    return true;
  }
  else
    return false;  
}

bool JointStrategy::operator<(const JointStrategy& other) const {
  //lets assume that the comparison is between joint actions of the
  //same set of players, otherwise this code is invalid
  /*
  map<short,strategy>::const_iterator self_begin=strats.begin();
  map<short,strategy>::const_iterator self_end=strats.end();
  map<short,strategy>::const_iterator other_begin=other.strats.begin();
  for(;self_begin!=self_end;self_begin++,other_begin++) {
    if(self_begin->second<other_begin->second)
      return true;
  }
  return false;
  */
  
  //We are assuming we are determining equality between joint_actions which
  //have same players in same order
  //we will compare the position of this joint_action in the cross product
  //to determine if they are the same
  if(cross_prod_pos<other.cross_prod_pos)
    return true;
  else
    return false;
}

JointStrategy* JointStrategy::get_projection(short i, short j) const {
  map<short,strategy>* p_strats=new map<short,strategy>();
  (*p_strats)[i]=((this->joint_action)->find(i))->second;
  (*p_strats)[j]=((this->joint_action)->find(j))->second;
  JointStrategy* projection=new JointStrategy(p_strats);
  return projection;  
}

void JointStrategy::calc_cross_prod_pos() {
  map<short,strategy>::const_reverse_iterator begin=joint_action->rbegin();
  map<short,strategy>::const_reverse_iterator end=joint_action->rend();
  short i=0;
  cross_prod_pos=0;
  for(;begin!=end;begin++) {
    cross_prod_pos+=((tuple_index) pow((GRID_SIZE+1),i))*(begin->second);
    i++;
  }
}

//get the joint strategy given the cross product position
void JointStrategy::pop_strat_from_cross_prod_pos(size_t k,vector<short>& players) {
  short i=players.size()-1;
  short j=i;
  for(;i>=1;i--) {
    (*joint_action)[players[j-i]]=(strategy) (k/((size_t) pow(GRID_SIZE+1,i)));
    k%=((size_t) pow(GRID_SIZE+1,i));
  }
  (*joint_action)[players[j-i]]=(strategy) k;
}

//we are assuming that the other joint action is totally disjoint from this one
void JointStrategy::combine_strats(JointStrategy& other) {
  joint_action->insert(other.joint_action->begin(),other.joint_action->end());
}

void JointStrategy::set_player_strat(strategy strat,short player_index) {
  (*joint_action)[player_index]=strat;
}

void JointStrategy::print_j_strat() const {
  map<short,strategy>::const_iterator begin=joint_action->begin();
  map<short,strategy>::const_iterator end=joint_action->end();
  cout << "[" << joint_action << " ";
  for(;begin!=end;begin++) {
    cout << "(" << begin->first << " " << (int) begin->second << ") ";
  }
  cout << cross_prod_pos << "]" << endl;
}

JointStrategy::~JointStrategy() {
  if(joint_action!=nullptr) {
    joint_action->erase(joint_action->begin(),joint_action->end());
    delete joint_action;
  }
}
