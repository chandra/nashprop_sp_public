%{
#include <string>
#include "Player.hpp"
#include "parser.hpp"
extern void yyerror(std::string error);
%}
%x COMMENT

%%

[\t\n]	;
" "     ;

"/*"	BEGIN(COMMENT);

<COMMENT>"*/"	BEGIN(INITIAL);
<COMMENT>.	;
"="	return EQUAL;
"("	return OPEN_PAREN;
")"	return CLOSE_PAREN;
"{"	return OPEN_BRACE;
"}"	return CLOSE_BRACE;
","	return COMMA;
";"	return SEMICOLON;
"["	return OPEN_SQ_BRKT;
"]"	return CLOSE_SQ_BRKT;
"num_players"	return NUM_PLAYERS;
"E"	return EDGE_SET_NAME;
"M"	return MATRIX_NAME;
"TAU"	return TAU;
"GRIDSIZE"	return GRIDSIZE;
"EPSILON"	return EPSILON;
"num_rounds"	return NUM_ROUNDS;
[0-9]*\.[0-9]+	{yylval.float_value=atof(yytext);return FLOAT;}
[0-9]+	{yylval.int_value=atoi(yytext);return INT;}
.	{std::string err_msg("Unknown token: +");
err_msg+=yytext;
err_msg+="+\n";
yyerror(err_msg);}

%%
int yywrap() {
return 1;
}