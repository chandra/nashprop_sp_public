#include <iostream>
#include <cmath>
#include <stdexcept>
#include "JointStrategySet.hpp"

using namespace std;

JointStrategySet::JointStrategySet() {
  strats_set=new list<JointStrategy*>();
  hash_value=0;
}

void JointStrategySet::calc_hash_value() {
  //we are going to calculate this only for sets whose members are joint strategies of two players, that is those sets for which messages are being passed
  //hash of a set=SUM(2^(joint_strategy->cross_prod_pos)), over all joint strategies in the set
  list<JointStrategy*>::const_iterator first=strats_set->begin();
  list<JointStrategy*>::const_iterator last=strats_set->end();
  hash_value=0;
  //iterating over the joint strategies
  for(;first!=last;first++) {
    hash_value+=(subset_index)pow(2,(*first)->cross_prod_pos);
  }
  if(hash_value==0)
    throw overflow_error("grid size too big");
}

JointStrategySet* JointStrategySet::get_projection(short i, short j) {
  JointStrategySet* projection=new JointStrategySet();
  list<JointStrategy*>::const_iterator first=strats_set->begin();
  list<JointStrategy*>::const_iterator last=strats_set->end();
  map<tuple_index,bool> projs_obtained;//to keep track of already seen tuple projections
  for(;first!=last;first++) {
    JointStrategy* proj=(*first)->get_projection(i,j);
    if(projs_obtained.find(proj->cross_prod_pos)==projs_obtained.end()) {
      projection->add_strategy(proj);
      projs_obtained[proj->cross_prod_pos]=true;
    }
    else
      delete proj;
  }
  projection->calc_hash_value();
  return projection;
}

JointStrategySet* JointStrategySet::get_projection(short i, short j,list<JointStrategy*>::const_iterator first,list<JointStrategy*>::const_iterator last) {
  JointStrategySet* projection=new JointStrategySet();
  map<tuple_index,bool> projs_obtained;//to keep track of already seen tuple projections
  for(;first!=last;first++) {
    JointStrategy* proj=(*first)->get_projection(i,j);
    //proj->print_j_strat();
    if(projs_obtained.find(proj->cross_prod_pos)==projs_obtained.end()) {
      projection->add_strategy(proj);//doesnt exist, so add it
      projs_obtained[proj->cross_prod_pos]=true;
    }
    else
      delete proj;
  }
  projection->calc_hash_value();
  return projection;
}


void JointStrategySet::add_strategy(JointStrategy* j_strat) {
  strats_set->push_back(j_strat);
}

void JointStrategySet::print_set() const {
  list<JointStrategy*>::const_iterator first=strats_set->begin();
  list<JointStrategy*>::const_iterator last=strats_set->end();
  cout << "{";
  for(;first!=last;first++) {
    (*first)->print_j_strat();
    cout << ",";
  }
  cout << hash_value << "}" << endl;
}

void JointStrategySet::print_set(std::list<JointStrategy*>::iterator first,std::list<JointStrategy*>::iterator last) const {
  cout << "{";
  for(;first!=last;first++) {
    (*first)->print_j_strat();
    cout << ",";
  }
  cout << hash_value << "}" << endl;
}

JointStrategySet::~JointStrategySet() {
  if(strats_set!=nullptr) {
    list<JointStrategy*>::iterator first=strats_set->begin();
    while(first!=strats_set->end()) {
      delete *first;
      first++;
    }
    strats_set->erase(strats_set->begin(),strats_set->end());
    delete strats_set;
  }
}
