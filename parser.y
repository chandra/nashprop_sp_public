%{
#include <utility>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <unordered_set>
#include <iterator>
#include <typeinfo>
#include "Player.hpp"

  //#define PARSE_DEBUG
  using namespace std;

  extern int num_players;
  extern Player* players;
  extern vector<pair<short, short> > edges;
  extern double tau;
  extern double gridsize;
  extern int num_rounds;
  extern double BR_EPSILON;
  vector<double>* payoff_matrix;
  int payoff_index;
  extern int yylex(void);
  void yyerror(string);
  void set_nbrs(vector<pair<short,short> > edges);

  /*
    Given a list containing all the edges between pair of players, for each edge,
    this function sets for each player in the edge, the other one as its neighbour
  */
  void set_nbrs(vector<pair<short,short> > edges) {
    vector<pair<short,short> >::const_iterator first=edges.begin();
    vector<pair<short,short> >::const_iterator last=edges.end();
    Player *player1, *player2;
    int i;
    //setting up the nbrs for each player, from the given edges
    for(;first!=last;first++) {
      pair<short,short> edge=*first;
      player1=&players[edge.first];
      player2=&players[edge.second];
      player1->nbrs.push_back(edge.second);
      player2->nbrs.push_back(edge.first);
#ifdef PARSE_DEBUG
      cout << "edge exists between " << player1->index << " and " << player2->index << endl;
      cout << "latest nbr added for player " << player1->index << " is " << player1->nbrs.back()
	   << endl;

      cout << "latest nbr added for player " << player2->index << " is " << player2->nbrs.back()
	   << endl;
#endif
    }
    //sort the neighbours for each player into an ascending order
    for(i=0;i<=num_players-1;i++) {
#ifdef PARSE_DEBUG
      cout << "nbrs before sorting for player " << players[i].index << " are: " << endl;
      std::copy(players[i].nbrs.begin(),players[i].nbrs.end(),
		std::ostream_iterator<int>(std::cout," "));
      cout << endl;
#endif
      sort(players[i].nbrs.begin(),players[i].nbrs.end());
      players[i].unset_nbrs_shuffled.insert(players[i].unset_nbrs_shuffled.begin(),
					    players[i].nbrs.begin(),players[i].nbrs.end());
#ifdef PARSE_DEBUG
      cout << "nbrs after sorting for player " << i << " are: " << endl;
      std::copy(players[i].nbrs.begin(),players[i].nbrs.end(),
		std::ostream_iterator<int>(std::cout," "));
      cout << endl;
      cout << "num_nbrs for player " << i << " is: " << players[i].num_nbrs << endl;
#endif
    }
  }
%}

%union {
  int int_value;
  double float_value;
  Player* player;
}
%token EQUAL SEMICOLON COMMA OPEN_SQ_BRKT CLOSE_SQ_BRKT OPEN_BRACE CLOSE_BRACE OPEN_PAREN CLOSE_PAREN
%token <int_value> INT
%token <float_value> FLOAT
%token NUM_PLAYERS EDGE_SET_NAME MATRIX_NAME TAU GRIDSIZE EPSILON NUM_ROUNDS

%type <player> matrix_row_lhs

%start game_def

%%

game_def : num_player_stmt edge_stmt payoff_stmt_list discretisation_stmt epsilon_stmt num_rounds_stmt;
num_player_stmt : NUM_PLAYERS EQUAL INT SEMICOLON
{
  //set the number of players
  num_players=$3;
  players=new Player[num_players];
  //  cout << "players is: " << players << endl;
  for(int i=0;i<=num_players-1;i++) {
    players[i].index=i;
  }
};
edge_stmt : EDGE_SET_NAME EQUAL edge_set SEMICOLON
{
  //Setting neighbours for players after determining the edges
  set_nbrs(edges);
};
edge_set : OPEN_BRACE edge_list CLOSE_BRACE;
edge_list : edge COMMA edge_list | edge ;
edge : OPEN_PAREN INT COMMA INT CLOSE_PAREN
{
  //create the edge (i,j)
  //add it to the edges map, with num_players*i+j as the key
  //when you want to know if there is an edge between i and j, search
  //in the edges map for keys num_players*i+j and num_players*j+i, if both return null
  //there is no edge (i,j)
#ifdef PARSE_DEBUG
  std::cout << "edge between " << $2 << " and " << $4 << "\n";
#endif
  std::pair<short,short> edge((int)$2,(int)$4);
  edges.push_back(edge);
  players[$2].num_nbrs=players[$2].num_nbrs+1;
  players[$4].num_nbrs=players[$4].num_nbrs+1;
};
payoff_stmt_list : payoff_stmt payoff_stmt_list | payoff_stmt;
payoff_stmt : matrix_row_lhs EQUAL
{
  payoff_matrix=new std::vector<double>();
}
matrix_row_rhs SEMICOLON
{
  $1->payoff=payoff_matrix;
};
matrix_row_lhs : MATRIX_NAME OPEN_SQ_BRKT INT CLOSE_SQ_BRKT
{
  $$=&(players[$3]);
};
matrix_row_rhs : OPEN_BRACE payoff_value_list CLOSE_BRACE;
payoff_value_list : INT
{
  payoff_matrix->push_back($1);
}
COMMA payoff_value_list
| INT
{
  payoff_matrix->push_back($1);
}
| FLOAT
{
  payoff_matrix->push_back($1);
}
COMMA payoff_value_list
| FLOAT
{
  payoff_matrix->push_back($1);
}
discretisation_stmt : tau_stmt | gridsize_stmt;
tau_stmt : TAU EQUAL FLOAT SEMICOLON
{
  tau=$3;
};
gridsize_stmt : GRIDSIZE EQUAL INT SEMICOLON
{
  gridsize=$3;
};
epsilon_stmt : EPSILON EQUAL FLOAT SEMICOLON
{
  BR_EPSILON=$3;
};
num_rounds_stmt : NUM_ROUNDS EQUAL INT SEMICOLON
{
  num_rounds=$3;
}

%%
void yyerror(std::string error) {
  cerr << error << endl;
}
