#include <cstdio>
#include <cstdlib>
#include <map>
#include <utility>
#include <iostream>
#include <iterator>
#include <algorithm>
#include "Player.hpp"

#define PARSE_DEBUG 1

using namespace std;

extern int yyparse();
extern FILE* yyin;
int num_players;
Player* players;
vector<pair<short, short> > edges;
double tau=-1;
double gridsize=-1;
int num_rounds;
double BR_EPSILON;
short GRID_SIZE=3;

int main(int argc, char** argv) {
  FILE *input=fopen(argv[1],"r");
  if(!input) {
    printf("Couldnt open file\n");
    exit(0);
  }
  yyin=input;
  yyparse();
  fclose(input);
  
  cout << "num of players is " << num_players << endl;
  cout << "tau is " << tau << endl;
  cout << "gridsize is " << gridsize << endl;
  cout << "num_rounds is " << num_rounds << endl;
  cout << "epsilon is " << BR_EPSILON << endl;
  int i=0;
  cout << "*******printing players*********";
  for(;i<=num_players-1;i++) {
    cout << "index is " << players[i].index << endl;
    cout << "strategy is " << (int) players[i].strat << endl;
    cout << "number of neighbours is " << players[i].num_nbrs << " " << players[i].nbrs.size() << endl;
    cout << "nbrs are " << endl;
    copy(players[i].nbrs.begin(),players[i].nbrs.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "unset nbrs are " << endl;
    copy(players[i].unset_nbrs_shuffled.begin(),players[i].unset_nbrs_shuffled.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "set nbrs are " << endl;
    copy(players[i].set_nbrs.begin(),players[i].set_nbrs.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "payoff size is " << players[i].payoff->size() << endl;
    cout << "payoffs are:" << endl;
    copy(players[i].payoff->begin(),players[i].payoff->end(),ostream_iterator<double>(cout," "));
    cout << endl;
    cout << "best response joint action set is:" << endl;
    players[i].B.print_set();
    cout << endl;
  }
}
