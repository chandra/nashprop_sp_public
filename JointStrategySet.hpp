#ifndef _JOINTSTRATEGYSET
#define _JOINTSTRATEGYSET
#include <list>
#include "JointStrategy.hpp"

/*
This class stores a set of joint strategies
 */
typedef unsigned short subset_index;

class JointStrategySet {
public:
  //set of joint strategies
  std::list<JointStrategy*>* strats_set;
  subset_index hash_value;
  JointStrategySet();  
  //get the projection of i,j from all tuples in this set
  JointStrategySet* get_projection(short i,short j);
  //get the projection of i,j from all tuples in given range
  JointStrategySet* get_projection(short i,short j,std::list<JointStrategy*>::const_iterator first,
				   std::list<JointStrategy*>::const_iterator last);
  //add a strategy to this set
  void add_strategy(JointStrategy* j_strat);
  void calc_hash_value();
  void print_set() const;
  void print_set(std::list<JointStrategy*>::iterator first,std::list<JointStrategy*>::iterator last) const;
  ~JointStrategySet();
};

#endif
