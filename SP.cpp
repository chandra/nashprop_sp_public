#include <iostream>
#include <map>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <random>
#include <algorithm>
#include <sstream>
#include <typeinfo>
#include "Player.hpp"
#include "PermComb.hpp"

//#define PARSE_DEBUG 1
#define PRINT_DEBUG 1

using namespace std;

extern int yyparse();
extern FILE* yyin;
int num_players;
//players
Player* players;
//edges between players in the graphical game
vector<pair<short, short> > edges;
double tau=-1;//interval length
int num_rounds;//how many rounds to iterate for
double BR_EPSILON;//approximation to BR
double gridsize=-1;//size of grid for mixed strategy
//We need an efficient way of retrieving the msgs passed from i->j, for a given T subset of A_ixA_j, where A_i is set of player i's strategies
//For efficiency, we will create a hash of the msgs with subsets as keys, if no hash, then message for that subset is 0
//key is just the hash value of that set
//storing all the msgs being passed around M(T)(i->j)
map<short,map<short,double*> > msgs[2];
//store the sum of the non-normalised msgs, which will be used as the normalisation factor
map<short,map<short,double> > msgs_sum;
short GRID_SIZE=1;
short CROSS_PROD_CARDINALITY=(GRID_SIZE+1)*(GRID_SIZE+1);
double COMPARISON_EPSILON=1e-8;
//TODO - change below vector to array for speedup
vector<short> unset_players_shuffled;//store in some random order all unset players
vector<short> set_players;//store the players whose strategies have been set
int old_msgs_index=0;
int new_msgs_index=1;

void destroy_SP();
void print_beta_msgs(int new_msgs_index,int old_msgs_index);
void nashprop_sp();
void init_msgs();
//void normalise_msgs(int msgs_index);
void print_debug_stmt(string stmt);
void calc_BR_sets();
void init_nashprop_sp();

template <typename T>
string to_string(const T t) {
  ostringstream os;
  os << t;
  return os.str();
}

//functor used for beta message calculation for each subset of the joint action best response set
class beta {
  Player* player_i;
  Player* player_j;
public:
  beta(Player* i,Player* j) {
    player_i=i;
    player_j=j;
  }

  //This function calculates the beta messages. Look at the equations of the beta messages to understand this portion of code
  bool operator()(list<JointStrategy*>::iterator first, list<JointStrategy*>::iterator last)//this function is called for each subset of the joint action best response set
  {

#ifdef PRINT_DEBUG
    //    cout << "B is:" << endl;
    //    player_i->B.print_set();
    cout << "Subset of B is:" << endl;
    player_i->B.print_set(first,last);
#endif

    JointStrategySet *R_ij,*T,*R_ki;
    R_ij=T=player_i->B.get_projection(player_i->index,player_j->index,first,last);
    
    print_debug_stmt("Projection T from subset of B is:");
#ifdef PRINT_DEBUG
    T->print_set();
#endif
    
    print_debug_stmt("Hash value of T is "+to_string(T->hash_value));
    double sum_log_message_R_ki=0.0;
    bool msg_R_ki_is_0=false;
    for(short m=0;m<=player_i->num_nbrs-1;m++) {
      short player_k_index=(player_i->nbrs)[m];
      if(players[player_k_index].strat==-1) {
	if(player_k_index!=player_j->index) {
	  R_ki=player_i->B.get_projection(player_k_index,player_i->index,first,last);
	  print_debug_stmt("Hash value of R_ki is "+to_string(R_ki->hash_value));
	  /*
#ifdef PRINT_DEBUG
	  cout << "Projection R_ki from subset of B is:" << endl;
	  R_ki->print_set();
	  
	  cout << "Beta msg from "<< player_k_index << " for R_ki is " << (msgs[old_msgs_index][player_k_index][player_i->index])[R_ki->hash_value-1] << endl;
#endif
	  */
    	  //TODO - is below check for zero OK?
	  if((msgs[old_msgs_index][player_k_index][player_i->index])[R_ki->hash_value-1]==0.0) {
	    msg_R_ki_is_0=true;
	    break;
	  }
	  else
	    sum_log_message_R_ki+=log((msgs[old_msgs_index][player_k_index][player_i->index])[R_ki->hash_value-1]);
	}
      }
    }
    if(!msg_R_ki_is_0) {
      double x=exp(sum_log_message_R_ki);
      if((T->strats_set->size()+distance(first,last))%2==0) {
#ifdef PRINT_DEBUG
	cout << "Value added to msg is:" << x << endl;
#endif
	(msgs[new_msgs_index][player_i->index][player_j->index])[T->hash_value-1]+=x;
	msgs_sum[player_i->index][player_j->index]+=x;
      }
      else {//TODO - the message cannot be a -ve value ever, any approximation should be aware of this restriction
#ifdef PRINT_DEBUG
	cout << "Value subtracted from msg is:" << x << endl;
#endif
	(msgs[new_msgs_index][player_i->index][player_j->index])[T->hash_value-1]-=x;
	msgs_sum[player_i->index][player_j->index]-=x;
      }
      print_debug_stmt("message currently is "+to_string((msgs[new_msgs_index][player_i->index][player_j->index])[T->hash_value-1]));
    }
    delete T;
    delete R_ki;
    return false;
  }
};

void print_debug_stmt(string stmt) {
#ifdef PRINT_DEBUG
  cout << stmt << endl;
#endif
}

/*
//This function normalises the messages
void normalise_msgs(int msgs_index) {
  vector<short>::const_iterator p_begin=unset_players_shuffled.begin();
  vector<short>::const_iterator p_end=unset_players_shuffled.end();
  for(;p_begin!=p_end;p_begin++) {
    short i=*p_begin;
    vector<short>::const_iterator n_begin=players[i].unset_nbrs_shuffled.begin();
    vector<short>::const_iterator n_end=players[i].unset_nbrs_shuffled.end();
    for(;n_begin!=n_end;n_begin++) {
      short j=*n_begin;
      //TODO - can we do a epsilon comparison with 0 for the normalisation factor?
      for(subset_index k=1;k<=(subset_index)pow(2,CROSS_PROD_CARDINALITY)-1;k++) {
	(msgs[msgs_index][i][j])[k-1]=exp(log((msgs[msgs_index][i][j])[k-1])-log(msgs_sum[i][j]));
      }
    }
  }
}
*/

//This function initialises all msgs to random values between 0.0 and 1.0
void init_msgs(int old_msgs_index) {
  uniform_real_distribution<double> msg_dist(0.0,1.0);
  random_device rd;
  mt19937_64 engine(1993764);//TODO - use rd() for seed later
  vector<short>::const_iterator p_begin=unset_players_shuffled.begin();
  vector<short>::const_iterator p_end=unset_players_shuffled.end();
  vector<double> rand_nums((subset_index) pow(2,CROSS_PROD_CARDINALITY));
  subset_index k;
  subset_index num_subsets=(subset_index) (pow(2,CROSS_PROD_CARDINALITY)-1);
  for(;p_begin!=p_end;p_begin++) {
    short i=*p_begin;
    print_debug_stmt("Intialising beta msgs for "+to_string(i));
    vector<short>::const_iterator n_begin=players[i].unset_nbrs_shuffled.begin();
    vector<short>::const_iterator n_end=players[i].unset_nbrs_shuffled.end();
    for(;n_begin!=n_end;n_begin++) {
      short j=*n_begin;
      print_debug_stmt("Intialising beta msgs for nbr "+to_string(j));
      msgs_sum[i][j]=0.0;
      rand_nums[0]=0.0;
      for(k=1;k<=num_subsets-1;k++) {
	rand_nums[k]=msg_dist(engine);
      }
      rand_nums[k]=1.0;
      sort(rand_nums.begin(),rand_nums.end());
      copy(rand_nums.begin(),rand_nums.end(),ostream_iterator<double>(cout," "));
      cout << endl;
      for(k=1;k<=num_subsets;k++) {
	rand_nums[k-1]=rand_nums[k]-rand_nums[k-1];
      }
      sort(rand_nums.begin(),rand_nums.end()-1);
      copy(rand_nums.begin(),rand_nums.end(),ostream_iterator<double>(cout," "));
      cout << endl;
      //by the very definition of the messages, value of the messages for subsets with
      //higher index are lower than those for subsets with lower index
      for(k=1;k<=num_subsets;k++) {
	(msgs[old_msgs_index][i][j])[k-1]=rand_nums[num_subsets-k];
	msgs_sum[i][j]+=(msgs[old_msgs_index][i][j])[k-1];
	cout << "message is " << (msgs[old_msgs_index][i][j])[k-1] << endl;
	cout << "message sum is " << msgs_sum[i][j] << endl;
      }
      //no need to normalise here because we are constructing these messages such that their sum is 1
      /*
      //normalise
      print_debug_stmt("Normalising beta msgs");
      for(subset_index k=1;k<=(subset_index)(pow(2,CROSS_PROD_CARDINALITY)-1);k++) {
	(msgs[old_msgs_index][i][j])[k-1]=exp(log((msgs[old_msgs_index][i][j])[k-1])-log(msgs_sum[i][j]));
	cout << "message is " << (msgs[old_msgs_index][i][j])[k-1] << endl;
      }
      */
    }
  }
}

//This function initialises the SP algorithm
void init_nashprop_sp() {
  //Initialise the list of unset players
  print_debug_stmt("Initialising list of unset players");
  for(int i=0;i<=num_players-1;i++) {
    unset_players_shuffled.push_back(i);
  }

  print_debug_stmt("Allocating memory for the beta msgs for all players");
  //allocate memory for the beta msgs
  vector<short>::const_iterator p_begin=unset_players_shuffled.begin();
  vector<short>::const_iterator p_end=unset_players_shuffled.end();
  vector<short>::const_iterator n_begin;
  vector<short>::const_iterator n_end;
  for(;p_begin!=p_end;p_begin++) {
    short i=*p_begin;
    n_begin=players[i].unset_nbrs_shuffled.begin();
    n_end=players[i].unset_nbrs_shuffled.end();
    for(;n_begin!=n_end;n_begin++) {
      short j=*n_begin;
      print_debug_stmt("Allocating mem for beta msgs between "+to_string(i)+" and "+to_string(j));
      msgs[0][i][j]=new double[(size_t) (pow(2,CROSS_PROD_CARDINALITY)-1)];
      msgs[1][i][j]=new double[(size_t) (pow(2,CROSS_PROD_CARDINALITY)-1)];
    }
  }
}

//This function executes the actual survey propagation version of the Nashprop algorithm
void nashprop_sp() {
  //while there is an unset player
  //calc Best response joint action set for each player
  //until no more rounds
  //randomise the list of unset players
  //for each unset player i
  //randomise the list of unset nbrs
  //for each unset neighbour j
  //pick subset R from B_i of size k
  //get projection R_ij=T
  //for each neighbour k!=j of i,
  //get projection R_ki
  //get beta(R_ki)(k->i)
  //calc beta(T)i->j
  //check if msgs converged
  //if trivial, exit and use some other algo
  //else, set a player and start over again

  vector<short>::const_iterator p_begin;
  vector<short>::const_iterator p_end;
  vector<short>::const_iterator n_begin;
  vector<short>::const_iterator n_end;
  while(!unset_players_shuffled.empty()) {//iterate until all players' strategy has been set
    bool trivial_msgs=true;
    for(int k=1;k<=num_rounds;k++) {//for the specified number of rounds
      print_debug_stmt("Round "+to_string(k));
      //swap old with new of previous round
      old_msgs_index=(old_msgs_index+1)%2;
      new_msgs_index=(new_msgs_index+1)%2;
      //initialise randomly if this is the first round
      if(k==1) {
	print_debug_stmt("Initialising messages randomly");
	init_msgs(old_msgs_index);
      }
      //shuffle the players around
      print_debug_stmt("Randomising the list of unset players");
      random_shuffle(unset_players_shuffled.begin(),unset_players_shuffled.end());

      p_begin=unset_players_shuffled.begin();
      p_end=unset_players_shuffled.end();
      bool converged=true;
      print_debug_stmt("Calculating beta msgs for all players");
      for(;p_begin!=p_end;p_begin++) {//iterate over all unset players
	Player *i=&players[*p_begin];
	print_debug_stmt("Calculating beta msgs for player "+to_string(i->index));
	print_debug_stmt("Randomising list of neighbours");
	//shuffle the neighbours around
	random_shuffle(i->unset_nbrs_shuffled.begin(),i->unset_nbrs_shuffled.end());
	n_begin=i->unset_nbrs_shuffled.begin();
	n_end=i->unset_nbrs_shuffled.end();
	for(;n_begin!=n_end;n_begin++) {//iterate over all unset neighbours
	  Player *j=&players[*n_begin];
	  print_debug_stmt("Calculating beta msgs being sent to nbr "+to_string(j->index));
	  //initialise the messsages from i->j and their normalisation factor to 0;
	  //TODO - if we could only go over the subsets that actually exist in i->B, that would be efficient. this can be done, record the hash_value of the subsets as you encounter them while msg creation
	  msgs_sum[i->index][j->index]=0.0;
	  for(subset_index index=1;index<=(subset_index) (pow(2,CROSS_PROD_CARDINALITY)-1);index++) {
	    (msgs[new_msgs_index][i->index][j->index])[index-1]=0.0;
	  }
	  print_debug_stmt("Iterating over all subsets of B");
	  list<JointStrategy*>::iterator it=i->B.strats_set->begin();
	  //calculate the msgs from i->j for all subsets T of A_ixA_j that are in i->B
	  for(size_t m=1;m<=i->B.strats_set->size();m++) {//for non-empty subsets of all sizes
	    print_debug_stmt("Iterating over subsets of size "+to_string(m));
	    it++;
	    for_each_combination(i->B.strats_set->begin(),it,i->B.strats_set->end(),beta(i,j));
	    
	    if(m==6) {
	      destroy_SP();
	      exit(0);
	    }
	    
	    
	    cout << "printing B" << endl;
	    i->B.print_set();
	    
	  }
	  //normalise and check if the msgs converged
	  print_debug_stmt("Normalising and checking for convergence of beta msgs");
	  print_debug_stmt("Normalisation factor for beta msgs is "+to_string(msgs_sum[i->index][j->index]));
	  if(abs(msgs_sum[i->index][j->index]-0.0)>COMPARISON_EPSILON) {
	    trivial_msgs=false;
	    for(subset_index index=1;index<=(subset_index) (pow(2,(GRID_SIZE+1)*(GRID_SIZE+1))-1);index++) {
	      print_debug_stmt("Message for subset with index "+to_string(index)+" is "+to_string((msgs[new_msgs_index][i->index][j->index])[index-1]));
	      (msgs[new_msgs_index][i->index][j->index])[index-1]=exp(log((msgs[new_msgs_index][i->index][j->index])[index-1])-log(msgs_sum[i->index][j->index]));
	      print_debug_stmt("Message for subset with index "+to_string(index)+" is "+to_string((msgs[new_msgs_index][i->index][j->index])[index-1]));
	      if(abs((msgs[old_msgs_index][i->index][j->index])[index-1]-(msgs[new_msgs_index][i->index][j->index])[index-1])>COMPARISON_EPSILON)
		converged=false;
	    }
	  }
	  else {
	    print_debug_stmt("Beta messages are trivial");
	  }
	}
      }
      //if msgs have converged, goto decimation step
      if(converged) {
	print_debug_stmt("All beta msgs have converged");
	if(trivial_msgs) {
	  print_debug_stmt("All beta msgs are trivial");
	  //TODO - convert to SAT and run local search
	}
	else {
	  print_debug_stmt("Not all beta msgs are trivial");
	  //TODO - set strategy for the most likely player
	}
	print_beta_msgs(new_msgs_index,old_msgs_index);
	break;
      }
    }
  }
}

void print_beta_msgs(int new_msgs_index,int old_msgs_index) {
#ifdef PRINT_DEBUG
  vector<short>::const_iterator p_begin=unset_players_shuffled.begin();
  vector<short>::const_iterator p_end=unset_players_shuffled.end();
  for(;p_begin!=p_end;p_begin++) {
    cout << "*******Messages from player " << *p_begin << " to all nbrs******" << endl;
    short i=*p_begin;
    vector<short>::const_iterator n_begin=players[i].unset_nbrs_shuffled.begin();
    vector<short>::const_iterator n_end=players[i].unset_nbrs_shuffled.end();
    for(;n_begin!=n_end;n_begin++) {
      cout << "***Messages to nbr " << *n_begin << "***" << endl;
      short j=*n_begin;
      msgs_sum[i][j]=0.0;
      for(subset_index k=1;k<=(subset_index)pow(2,CROSS_PROD_CARDINALITY)-1;k++) {
	cout << "Old message for subset " << k << " is " << msgs[old_msgs_index][i][j][k-1] << endl;
	cout << "New message for subset " << k << " is " << msgs[new_msgs_index][i][j][k-1] << endl;
      }
    }
  }
#endif
}

int main(int argc, char** argv) {
  FILE *input=fopen(argv[1],"r");
  if(!input) {
    printf("Couldnt open file\n");
    exit(0);
  }
  yyin=input;
  yyparse();
  fclose(input);

  cout << "*******GAME INFO BEGIN******" << endl;
  cout << "num of players is " << num_players << endl;
  cout << "tau is " << tau << endl;
  cout << "gridsize from file is " << gridsize << endl;
  cout << "overriding gridsize from file, new gridsize is " << GRID_SIZE << endl;
  cout << "num_rounds is " << num_rounds << endl;
  cout << "epsilon is " << BR_EPSILON << endl;
  cout << "*******GAME INFO END******" << endl << endl;
#ifdef PARSE_DEBUG
  cout << "*******printing players*********";
  for(int i=0;i<=num_players-1;i++) {
    cout << "index is " << players[i].index << endl;
    cout << "strategy is " << (int) players[i].strat << endl;
    cout << "number of neighbours is " << players[i].num_nbrs << " " << players[i].nbrs.size() << endl;
    cout << "nbrs are " << endl;
    copy(players[i].nbrs.begin(),players[i].nbrs.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "unset nbrs are " << endl;
    copy(players[i].unset_nbrs_shuffled.begin(),players[i].unset_nbrs_shuffled.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "set nbrs are " << endl;
    copy(players[i].set_nbrs.begin(),players[i].set_nbrs.end(),ostream_iterator<short>(cout," "));
    cout << endl;
    cout << "payoff size is " << players[i].payoff->size() << endl;
    cout << "payoffs are:" << endl;
    copy(players[i].payoff->begin(),players[i].payoff->end(),ostream_iterator<double>(cout," "));
    cout << endl;
  }
#endif
  calc_BR_sets();
  init_nashprop_sp();
  nashprop_sp();
  destroy_SP();
}

void calc_BR_sets() {
  print_debug_stmt("Computing joint action BR sets for all players");
  //Compute joint action BR sets for all players
  for(int i=0;i<=num_players-1;i++) {
    print_debug_stmt("Computing joint action BR set for player "+to_string(i));
    players[i].pop_joint_action_BR_set(players);
    players[i].B.print_set();
  }
}

void destroy_SP() {
  //destroy memory allocated for the beta msgs
  for(int i=0;i<=num_players-1;i++) {
    for(int j=0;j<=((players[i]).num_nbrs)-1;j++) {
      print_debug_stmt("Destroying mem allocated for beta msgs between "+to_string((players[i]).index)+" and "+to_string(((players[i]).nbrs)[j]));
      delete[] msgs[0][(players[i]).index][((players[i]).nbrs)[j]];
      delete[] msgs[1][(players[i]).index][((players[i]).nbrs)[j]];
    }
  }
  delete[] players;
}
