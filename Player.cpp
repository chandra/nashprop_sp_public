#include <cmath>
#include <bitset>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <iterator>
#include "Player.hpp"

using namespace std;
extern short GRID_SIZE;
extern double BR_EPSILON;

Player::Player() {
  index=-1;
  strat=-1;
  num_nbrs=0;
  payoff=0;
}

//This function will return the set containing all the joint strategies that are epsilon BR for player i
//Instead of representing the joint action as a tuple, i think we can represent it as a number, this will lead to less mem, but it might lead to slower speed, bcoz the actual strategies hvae to be computed agin and again from the number, thats a trade-off
//TODO - there are some == comparisons of doubles, when doing >=, without using a delta difference for te comparison
//TODO - the way we are calculating this BR set, there are restrictions that pow(GRID_SIZE+1,num_of_nbrs) should fit within size_t, else overflow errors occur
void Player::pop_joint_action_BR_set(Player* players) {
  vector<short>::const_iterator first=set_nbrs.begin();
  vector<short>::const_iterator last=set_nbrs.end();
  //create a joint strategy of nbrs who have already played
  JointStrategy set_nbrs_strat;
  for(;first!=last;first++) {
    set_nbrs_strat.set_player_strat(players[*first].strat,*first);
  }
  size_t num_j_strats=(size_t) pow(GRID_SIZE+1,unset_nbrs_shuffled.size());
  if(num_j_strats==0) {
    ostringstream os;
    os << index;
    string error_msg="cant handle this many neighbours for player ";
    error_msg+=os.str()+" at this grid size";
    throw overflow_error(error_msg);
  }
  JointStrategy j_strat_nbrs;
  //for each possible joint strategy of the unset nbrs
  for(size_t k=0;k<=num_j_strats-1;k++) {
    j_strat_nbrs.pop_strat_from_cross_prod_pos(k,unset_nbrs_shuffled);
    cout << k << " ";
    j_strat_nbrs.print_j_strat();
    j_strat_nbrs.combine_strats(set_nbrs_strat);
    tuple<short,short> br_i=get_BR_i(&j_strat_nbrs);
    //add the br strats to the br set
    for(short m=get<0>(br_i);m<=get<1>(br_i);m++) {
      JointStrategy* j_strat_all=new JointStrategy(j_strat_nbrs);
      (*(j_strat_all->joint_action))[index]=(strategy) m;
      j_strat_all->print_j_strat();
      B.add_strategy(j_strat_all);
    }
  }
}

inline int get_int_player_strat(JointStrategy *j_strat,short player_index) {
  return (int) j_strat->get_player_strat(player_index);
}

/*
  This function returns the best response for player i given the actions of the neighbours.
  j_strat is joint action of all nbrs
  returns interval [a,b] where BR is all grid points in that interval
  //TODO - get rid of the bitset for efficiency?
  */
tuple<short,short> Player::get_BR_i(JointStrategy* j_strat) {
  size_t num_of_pure_payoffs=payoff->size();
  double prob=0.0;
  size_t k=0;
  double payoff_0,payoff_1;
  payoff_1=payoff_0=0.0;
  bool prob_is_0=false;//prob of a particular pure joint action of all nbrs being 0 in the given mixed joint action
  for(;k<=num_of_pure_payoffs-1;k=k+2) {//for all possible joint pure actions
    bitset<64> pure_joint_action(k);
    prob_is_0=false;
    prob=0.0;
    for(int j=1;j<=num_nbrs;j++) {//starting from nbr who has highest player index to nbr with lowest player index, calc prob of this pure joint action in the mixed joint action
      int nbr_player_index=nbrs[num_nbrs-j];//this is because the payoff matrix index converted into bits is exact opposite order of how the nbrs are stored in i->nbrs
      int nbr_player_strat=get_int_player_strat(j_strat,nbr_player_index);
      if(pure_joint_action[j]) {//action of the nbr is 1 in this pure joint action
	if(nbr_player_strat==0) {//but prob in mixed joint strategy for action 1 is 0
	  prob_is_0=true;
	  break;
	}
	else
	  prob+=log((nbr_player_strat*1.0)/GRID_SIZE);
      }
      else {//pure action is 0
	if(nbr_player_strat==GRID_SIZE) {//but prob in mixed strategy for pure action 0 is 0
	  prob_is_0=true;
	  break;
	}
	else
	  prob+=log(1.0-((nbr_player_strat*1.0)/GRID_SIZE));
      }
    }
    if(!prob_is_0) {
      payoff_0+=exp(prob+log((*(payoff))[k]));
      payoff_1+=exp(prob+log((*(payoff))[k+1]));
    }
  }
  //now determine the epsilon BR interval
  //TODO - shouldn't we use COMPARISON_EPSILON for the = comparison in the >= and <= comparisons being done here?
  if(payoff_0<payoff_1) {
    if(payoff_0>=payoff_1-BR_EPSILON)
      return tuple<short,short>(0,GRID_SIZE);
    else {
      for(int k=1;k<=GRID_SIZE;k++) {
	if(((GRID_SIZE-k)*1.0/GRID_SIZE)*payoff_0+(k*1.0/GRID_SIZE)*payoff_1>=payoff_1-BR_EPSILON)
	  return tuple<short,short>(k,GRID_SIZE);
      }
    }
  }
  else if(payoff_0==payoff_1) {//all grid points are epsilon BR
    return tuple<short,short>(0,GRID_SIZE);
  }
  else {
    if(payoff_1>=payoff_0-BR_EPSILON)
      return tuple<short,short>(0,GRID_SIZE);
    else {
      for(int k=GRID_SIZE-1;k>=0;k--) {
	if(((k*1.0)/GRID_SIZE)*payoff_1+((GRID_SIZE-k)*1.0/GRID_SIZE)*payoff_0>=payoff_0-BR_EPSILON)
	  return tuple<short,short>(0,k);
      }
    }
  }
}

void Player::print_player() const {
  cout << "{" << index << "," << (int) strat << "," << num_nbrs << ",[";
  copy(nbrs.begin(),nbrs.end(),ostream_iterator<short>(cout," "));
  cout << "],[";
  copy(unset_nbrs_shuffled.begin(),unset_nbrs_shuffled.end(),ostream_iterator<short>(cout," "));
  cout << "],[";
  copy(set_nbrs.begin(),set_nbrs.end(),ostream_iterator<short>(cout," "));
  cout << "],[";
  copy(payoff->begin(),payoff->end(),ostream_iterator<double>(cout," "));
  cout << "],";
  B.print_set();
}

Player::~Player() {
  if(payoff!=nullptr)
    delete payoff;
}
