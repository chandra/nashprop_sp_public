#ifndef _JOINTSTRATEGY
#define _JOINTSTRATEGY
#include <map>
#include <vector>

typedef char strategy;
typedef short tuple_index;

/*
This class stores a tuple representing a joint mixed strategy (a_1,a_2,...,a_n) of n players
 */
class JointStrategy {
public:
  std::map<short,strategy>* joint_action;
  tuple_index cross_prod_pos;//will be calculated only for two player joint actions
  JointStrategy();
  JointStrategy(std::map<short,strategy>* joint_action);
  JointStrategy(const JointStrategy& j_strat);
  JointStrategy(JointStrategy&& j_strat);
  JointStrategy& operator=(const JointStrategy& other);
  JointStrategy& operator=(JointStrategy&& other);
  //returns strategy of the player in this joint strategy
  strategy get_player_strat(short player) const;
  //returns true if both these joint strategies are the same
  bool operator==(const JointStrategy& other) const;
  //returns true if this joint strategy is less than the other
  bool operator<(const JointStrategy& other) const;
  //returns projection of i and j in this joint strategy
  JointStrategy* get_projection(short i, short j) const;  
  void calc_cross_prod_pos();
  void pop_strat_from_cross_prod_pos(std::size_t k,std::vector<short>& players);
  void combine_strats(JointStrategy& other);
  void set_player_strat(strategy strat,short player_index);
  void print_j_strat() const;
  ~JointStrategy();
};

#endif
